/*
 * gcc -fPIC -c -o clock_gettime.o clock_gettime.c
 * gcc -shared -o clock_gettime.so clock_gettime.o -ldl
 *
 * Use:
 * LD_PRELOAD="./clock_gettime.so" command
 * 
 * Author:
 * Vimal
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#define NSEC_PER_SEC (1000000000LL)

int (*_clock_gettime)(clockid_t clk_id, struct timespec *tp)=NULL;
int (*_timer_settime)(timer_t timerid, int flags,
                        const struct itimerspec *new_value,
                        struct itimerspec * old_value) = NULL;
static int tdf = 2;
static char *env = NULL;

int timer_settime(timer_t timerid, int flags, const struct itimerspec *new_value, struct itimerspec *old_value) {
    struct itimerspec dilated;

    if (_timer_settime == NULL) {
        env = getenv("TDF");
        tdf = env ? atoi(env) : 1;
        if (tdf < 1) tdf = 1;
        _timer_settime = dlvsym(RTLD_NEXT, "timer_settime", "GLIBC_2.3.3");
        printf("preloaded timer_settime @%p, using TDF=%d\n", _timer_settime, tdf);
    }

    if (new_value) {
        struct timespec ts = new_value->it_value;
        unsigned long long t = ts.tv_sec * NSEC_PER_SEC + ts.tv_nsec;
        t *= tdf;
        dilated.it_interval = new_value->it_interval;
        dilated.it_value = ts;
        return _timer_settime(timerid, flags, &dilated, old_value);
    }

    return _timer_settime(timerid, flags, new_value, old_value);
}


int clock_gettime(clockid_t clk_id, struct timespec *tp)
{
    unsigned long long t=0;
    struct timespec ltp;
    int ret;

    if(_clock_gettime == NULL) {
        env = getenv("TDF");
        tdf = env ? atoi(env) : 1;
        if(tdf < 1) {
            tdf = 1;
        }
        _clock_gettime = (int (*)(clockid_t, struct timespec *)) dlsym(RTLD_NEXT, "clock_gettime");
        printf("preloaded @%p clock_gettime, using TDF=%d\n", _clock_gettime, tdf);
    }

    ret = _clock_gettime(clk_id, &ltp);
    t = ltp.tv_sec * NSEC_PER_SEC + ltp.tv_nsec;
    t /= tdf;

    if(tp) {
        tp->tv_sec = t / NSEC_PER_SEC;
        tp->tv_nsec = t % NSEC_PER_SEC;
    }

    return ret;
}

