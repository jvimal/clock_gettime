
all: clock_gettime.c
	gcc -fPIC -c -o clock_gettime.o clock_gettime.c
	gcc -shared -o clock_gettime.so clock_gettime.o -ldl
